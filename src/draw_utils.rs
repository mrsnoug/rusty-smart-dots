use sdl2::{
    pixels::Color,
    rect::{Point, Rect},
    render::Canvas,
    video::Window,
};

use crate::dots::{Dot, Vector};

pub fn draw_obstacles(canvas: &mut Canvas<Window>, obstacles: &[Rect]) {
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.fill_rects(obstacles).unwrap();
}

pub fn draw_circle(canvas: &mut Canvas<Window>, center: &Vector, radius: i32, color: Color) {
    canvas.set_draw_color(color);

    let mut x = radius;
    let mut y = 0;
    let mut re = x * x + y * y - radius * radius;
    while x >= y {
        canvas
            .draw_point(Point::new((center.x as i32) + x, (center.y as i32) + y))
            .unwrap();
        canvas
            .draw_point(Point::new(center.x as i32 + y, center.y as i32 + x))
            .unwrap();

        canvas
            .draw_point(Point::new(center.x as i32 - x, center.y as i32 + y))
            .unwrap();
        canvas
            .draw_point(Point::new(center.x as i32 - y, center.y as i32 + x))
            .unwrap();

        canvas
            .draw_point(Point::new(center.x as i32 - x, center.y as i32 - y))
            .unwrap();
        canvas
            .draw_point(Point::new(center.x as i32 - y, center.y as i32 - x))
            .unwrap();

        canvas
            .draw_point(Point::new(center.x as i32 + x, center.y as i32 - y))
            .unwrap();
        canvas
            .draw_point(Point::new(center.x as i32 + y, center.y as i32 - x))
            .unwrap();

        if 2 * (re + 2 * y + 1) + 1 - 2 * x > 0 {
            re += 1 - 2 * x;
            x -= 1;
        }
        re += 2 * y + 1;
        y += 1;
    }
}

pub fn draw_dots(canvas: &mut Canvas<Window>, dots: &[Dot], radius: i32) {
    for dot in dots {
        let current_position = dot.get_position();
        let color = if dot.is_best() {
            Color::RGB(255, 0, 0)
        } else if dot.is_dead() {
            Color::RGB(255, 255, 0)
        } else {
            Color::RGB(0, 0, 255)
        };

        draw_circle(canvas, current_position, radius, color);
    }
}

pub fn draw_goal(canvas: &mut Canvas<Window>, goal: Vector, radius: i32) {
    draw_circle(canvas, &goal, radius, Color::RGB(0, 255, 0));
}
