use super::Vector;

pub const WIDTH: f64 = 1000.0;
pub const HEIGHT: f64 = 1000.0;

pub const NUM_DOTS: usize = 2000;
pub const BRAIN_SIZE: usize = 1000;
pub const INITIAL_MAX_STEPS: usize = 1000;
pub const MUTATION_RATE: f64 = 0.02;

pub const GOAL: Vector = Vector {
    x: WIDTH / 2.0,
    y: 15.0,
};

pub const STARTING_POINT: Vector = Vector {
    x: WIDTH / 2.0,
    y: HEIGHT - 50.0,
};
