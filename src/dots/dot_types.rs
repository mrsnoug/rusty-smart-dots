use rand::prelude::*;
use sdl2::rect::Rect;

use super::{BRAIN_SIZE, GOAL, HEIGHT, INITIAL_MAX_STEPS, MUTATION_RATE, STARTING_POINT, WIDTH};

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct Vector {
    pub(crate) x: f64,
    pub(crate) y: f64,
}

impl Into<(i32, i32)> for Vector {
    fn into(self) -> (i32, i32) {
        (self.x as i32, self.y as i32)
    }
}

impl Vector {
    pub fn new(x: f64, y: f64) -> Self {
        Self { x, y }
    }

    pub fn get_x(&self) -> f64 {
        self.x
    }

    pub fn get_y(&self) -> f64 {
        self.y
    }

    pub fn set_x(&mut self, x: f64) {
        self.x = x;
    }

    pub fn set_y(&mut self, y: f64) {
        self.y = y;
    }

    pub fn magnitude(&self) -> f64 {
        f64::sqrt(self.x * self.x + self.y * self.y)
    }

    pub fn add(&mut self, other: Vector) {
        self.x += other.x;
        self.y += other.y;
    }

    pub fn limit(&mut self, max_magnitude: f64) {
        let current_magnitude = self.magnitude();
        if current_magnitude > max_magnitude {
            let scale = current_magnitude / max_magnitude;
            self.x = self.x / scale;
            self.y = self.y / scale;
        }
    }

    pub fn vec_from_angle(angle: f64) -> Self {
        Self {
            x: angle.cos(),
            y: angle.sin(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Brain {
    directions: Vec<Vector>,
    step: usize,
}

impl Brain {
    pub fn new(size: usize) -> Self {
        let directions = Vec::with_capacity(size);
        let mut this = Self {
            directions,
            step: 0,
        };

        this.randomize(size);
        this
    }

    fn randomize(&mut self, path_len: usize) {
        let mut rng = rand::thread_rng();
        for _ in 0..path_len {
            let random_angle: f64 = std::f64::consts::PI * (2.0 * rng.gen::<f64>() - 1.0);
            self.directions.push(Vector::vec_from_angle(random_angle));
        }
    }

    pub fn mutate(&mut self) {
        let mut rng = rand::thread_rng();

        for i in 0..BRAIN_SIZE {
            let chance: f64 = rng.gen();
            if chance < MUTATION_RATE {
                let random_angle: f64 = std::f64::consts::PI * (2.0 * rng.gen::<f64>() - 1.0);
                self.directions[i] = Vector::vec_from_angle(random_angle);
            }
        }
    }

    pub fn get_step(&self) -> usize {
        self.step
    }

    pub fn set_step(&mut self, step: usize) {
        self.step = step;
    }

    pub fn get_directions(&self) -> Vec<Vector> {
        self.directions.clone()
    }

    pub fn get_directions_len(&self) -> usize {
        self.directions.len()
    }

    pub fn set_directions(&mut self, new_directions: Vec<Vector>) {
        self.directions = new_directions;
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Dot {
    position: Vector,
    velocity: Vector,
    acceleration: Vector,
    brain: Brain,
    is_dead: bool,
    reached_goal: bool,
    is_best: bool,
    fitness: f64,
}

impl Dot {
    pub fn new() -> Self {
        Self {
            position: Vector::new(STARTING_POINT.x, STARTING_POINT.y),
            velocity: Vector::new(0.0, 0.0),
            acceleration: Vector::new(0.0, 0.0),
            brain: Brain::new(BRAIN_SIZE),
            is_best: false,
            is_dead: false,
            reached_goal: false,
            fitness: 0.0,
        }
    }

    pub fn update(&mut self, obstacles: &[Rect]) {
        if self.is_dead || self.reached_goal {
            return;
        }

        self.move_dot();

        if self.position.x < 3.0
            || self.position.x > WIDTH - 3.0
            || self.position.y < 3.0
            || self.position.y > HEIGHT - 3.0
        {
            // bumbed into frame
            self.is_dead = true;
            return;
        }

        for obstacle in obstacles {
            if obstacle.contains_point(self.position) {
                self.is_dead = true;
                return;
            }
        }

        if self.distance_to_goal() < 10.0 {
            self.reached_goal = true;
        }
    }

    fn move_dot(&mut self) {
        let acceleration = if self.brain.get_directions_len() > self.brain.get_step() {
            let accel = self.brain.directions[self.brain.step];
            self.brain.step += 1;

            accel
        } else {
            self.is_dead = true;

            Vector::new(0.0, 0.0)
        };

        self.velocity.add(acceleration);
        self.velocity.limit(4.0);
        self.position.add(self.velocity);
    }

    pub fn produce_baby(&self) -> Self {
        let mut baby = Self::new();
        baby.brain = self.brain.clone();
        baby.brain.step = 0;

        baby
    }

    pub fn calculate_fitness(&mut self) {
        self.fitness = if self.reached_goal {
            (1.0 / 16.0) + 10000.0 / ((self.brain.get_step() * self.brain.get_step()) as f64)
        } else {
            let distance_to_goal = self.distance_to_goal();

            1.0 / (distance_to_goal * distance_to_goal)
        }
    }

    fn distance_to_goal(&self) -> f64 {
        let tmp = (GOAL.x - self.position.x).powf(2.0) + (GOAL.y - self.position.y).powf(2.0);

        tmp.sqrt()
    }

    pub fn set_best(&mut self, is_best: bool) {
        self.is_best = is_best;
    }

    pub fn is_best(&self) -> bool {
        self.is_best
    }

    pub fn set_dead(&mut self, is_dead: bool) {
        self.is_dead = is_dead;
    }

    pub fn is_dead(&self) -> bool {
        self.is_dead
    }

    pub fn get_position(&self) -> &Vector {
        &self.position
    }
}

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Population {
    dots: Vec<Dot>,
    fitness_sum: f64,
    generation: usize,
    best_dot_idx: usize,
    min_step: usize,
}

impl Population {
    pub fn new(size: usize) -> Self {
        let mut dots = Vec::with_capacity(size);
        for _ in 0..size {
            dots.push(Dot::new());
        }
        Self {
            dots,
            fitness_sum: 0.0,
            generation: 1,
            best_dot_idx: 0,
            min_step: INITIAL_MAX_STEPS,
        }
    }

    pub fn update(&mut self, obstacles: &[Rect]) {
        for idx in 0..self.dots.len() {
            let this_dot = self.dots.get_mut(idx).unwrap();
            if this_dot.brain.get_step() > self.min_step {
                this_dot.set_dead(true);
            } else {
                this_dot.update(obstacles)
            }
        }
    }

    pub fn calculate_all_fitnesses(&mut self) {
        for idx in 0..self.dots.len() {
            let this_dot = self.dots.get_mut(idx).unwrap();
            this_dot.calculate_fitness();
        }
    }

    pub fn mutate_dem_babies(&mut self) {
        for idx in 0..self.dots.len() {
            let this_dot = self.dots.get_mut(idx).unwrap();
            this_dot.brain.mutate();
        }
    }

    pub fn natural_selection(&mut self) {
        let mut new_dots: Vec<Dot> = Vec::with_capacity(self.dots.len());
        self.calculate_all_fitnesses();
        self.set_best_dot();
        self.calculate_fitness_sum();

        let mut best_dot = self.dots.get(self.best_dot_idx).unwrap().produce_baby();
        best_dot.set_best(true);
        new_dots.push(best_dot.clone());

        //assert_eq!(best_dot.brain.directions.len(), BRAIN_SIZE);

        for _ in 1..self.dots.len() {
            let parent = self.select_parent().unwrap();
            let new_dot = parent.produce_baby();
            new_dots.push(new_dot);
        }

        assert_eq!(self.dots.len(), new_dots.len());

        self.dots = new_dots;
        self.generation += 1;
    }

    pub fn are_all_dots_dead(&self) -> bool {
        for dot in self.dots.iter() {
            if !dot.is_dead && !dot.reached_goal {
                return false;
            }
        }

        true
    }

    fn set_best_dot(&mut self) {
        let mut max_fitness = 0.0;
        let mut best_dot_so_far = 0;

        for idx in 0..self.dots.len() {
            let this_dot = self.dots.get(idx).unwrap();
            if this_dot.fitness > max_fitness {
                max_fitness = this_dot.fitness;
                best_dot_so_far = idx;
            }
        }

        self.best_dot_idx = best_dot_so_far;
        let best_dot_this_gen = self.dots.get(self.best_dot_idx).unwrap();
        if best_dot_this_gen.reached_goal {
            self.min_step = best_dot_this_gen.brain.get_step();
        }
    }

    fn calculate_fitness_sum(&mut self) {
        self.fitness_sum = 0.0;
        for dot in self.dots.iter() {
            self.fitness_sum += dot.fitness;
        }
    }

    fn select_parent(&self) -> Option<Dot> {
        // this is almost as choosing the parent randomly
        let mut rng = rand::thread_rng();

        let random_threshold = rng.gen::<f64>() * self.fitness_sum;
        let mut running_sum = 0.0;

        for dot in self.dots.iter() {
            running_sum += dot.fitness;
            if running_sum >= random_threshold {
                return Some(dot.clone());
            }
        }

        None
    }

    pub fn get_generation(&self) -> usize {
        self.generation
    }

    pub fn get_min_step(&self) -> usize {
        self.min_step
    }

    pub fn get_dots(&self) -> &[Dot] {
        &self.dots
    }
}
