pub mod dots;
pub mod draw_utils;

extern crate sdl2;

use dots::{Population, GOAL, NUM_DOTS};
use draw_utils::{draw_dots, draw_goal, draw_obstacles};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use std::time::Duration;

pub fn main() {
    let obstacles = vec![
        Rect::new(0, 250, 600, 50),
        Rect::new(400, 650, 600, 50),
        Rect::new(480, 480, 40, 40),
    ];

    let mut population = Population::new(NUM_DOTS);

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("Smart Dots", 1000, 1000)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(255, 255, 255));
    canvas.clear();
    draw_obstacles(&mut canvas, &obstacles[..]);
    draw_goal(&mut canvas, GOAL, 10);
    draw_dots(&mut canvas, population.get_dots(), 2);
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                _ => {}
            }
        }

        if population.get_generation() > 250 {
            println!("Generation > 250, breaking...");
            break 'running;
        }

        if population.are_all_dots_dead() {
            population.natural_selection();
            population.mutate_dem_babies();
            println!(
                "Generation {} | Current fastest is {} steps",
                population.get_generation() - 1,
                population.get_min_step()
            );
        } else {
            population.update(&obstacles[..]);
            canvas.set_draw_color(Color::RGB(255, 255, 255));
            canvas.clear();
            draw_obstacles(&mut canvas, &obstacles[..]);
            draw_goal(&mut canvas, GOAL, 10);
            draw_dots(&mut canvas, population.get_dots(), 2);
        }

        canvas.present();
        ::std::thread::sleep(Duration::from_millis(5));
    }
}
