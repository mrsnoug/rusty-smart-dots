# Smart Dots

This is a very simple SDL2 project with the most basic genetic algorithm. Heavily inspired by CodeBullet's video. Done purely to kill time.

You'll need to have SDL2 installed and have it accessible as a dependency. On Mac, this should do the trick if you install SDL2 via brew:

```bash
export LIBRARY_PATH="$LIBRARY_PATH:$(brew --prefix)/lib"
```
